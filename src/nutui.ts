import '@nutui/nutui/dist/styles/themes/default.scss'
import "vue-search-select/dist/VueSearchSelect.css"

import { Avatar, Icon, Button, InfiniteLoading, List, Navbar, Switch, DatePicker, Picker, Swipe, Badge, SideNavBar, SubSideNavBar, SideNavBarItem } from '@nutui/nutui-taro'
import { Tabbar, TabbarItem } from '@nutui/nutui-taro'
import { Swiper, SwiperItem } from '@nutui/nutui-taro'
import { Cell, CellGroup } from '@nutui/nutui-taro'
import { Radio, RadioGroup } from '@nutui/nutui-taro'
import { Checkbox, CheckboxGroup } from '@nutui/nutui-taro'
import { Indicator } from '@nutui/nutui-taro'
import { Form, FormItem } from '@nutui/nutui-taro'
import { Input, InputNumber } from '@nutui/nutui-taro'
import { Address, Popup, Elevator, ActionSheet } from '@nutui/nutui-taro'
import { Dialog, OverLay } from '@nutui/nutui-taro'
import { Sku, Price } from '@nutui/nutui-taro'
import { NoticeBar } from '@nutui/nutui-taro'
import { Collapse, CollapseItem } from '@nutui/nutui-taro'
import { Divider } from '@nutui/nutui-taro'
import { Layout, Row, Col } from '@nutui/nutui-taro'
import { ImagePreview } from '@nutui/nutui-taro'
import { Card } from '@nutui/nutui-taro'
import { Tag } from '@nutui/nutui-taro'
import { Tabs, TabPane } from '@nutui/nutui-taro'
import { FixedNav } from '@nutui/nutui-taro'
import { Empty } from '@nutui/nutui-taro'
import { Image } from '@nutui/nutui-taro'
import { Grid, GridItem } from '@nutui/nutui-taro'
import { SearchBar } from '@nutui/nutui-taro';
import { Menu, MenuItem } from '@nutui/nutui-taro';
import { Calendar } from '@nutui/nutui-taro';

import { App } from 'vue'
const setNutUi = (app: App) => {
  app.use(Icon).use(Avatar)
  app.use(Tabbar).use(TabbarItem)
  app.use(Swiper).use(SwiperItem)
  app.use(Swipe)
  app.use(Cell)
  app.use(List)
  app.use(CellGroup)
  app.use(Radio).use(RadioGroup)
  app.use(CheckboxGroup).use(Checkbox)
  app.use(Indicator)
  app.use(Card)
  app.use(Tag).use(Badge)
  app.use(Form).use(FormItem)
  app
    .use(Input)
    .use(InputNumber)
    .use(Button)
  app
    .use(Address)
    .use(Popup)
    .use(Elevator)
  app.use(Dialog).use(OverLay)
  app.use(Sku).use(Price)
  app.use(NoticeBar)
  app.use(Collapse).use(CollapseItem)
  app.use(ActionSheet)
  app.use(Divider)
  app.use(ImagePreview)
  app
    .use(Layout)
    .use(Row)
    .use(Col)

  app.use(Tabs).use(TabPane)

  app.use(FixedNav)
  app.use(Empty)
  app.use(Image)

  app.use(SearchBar)
  app.use(Grid).use(GridItem)

  app.use(Menu)
  app.use(MenuItem)

  app.use(InfiniteLoading)

  app.use(Navbar).use(Switch)
  app.use(Calendar)
  app.use(DatePicker).use(Picker)
  app.use(SideNavBar).use(SubSideNavBar).use(SideNavBarItem);
}
export default setNutUi
