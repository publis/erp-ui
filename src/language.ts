import { createI18n } from 'vue-i18n'


//得到默认使用的语言
const getDefaultLang = () => {
    //用户指定了默认语言时，使用用户指定的
    const llang = localStorage.getItem('lang')
    if (llang && ['zh_cn','zh_tw','en'].includes(llang)) {
        return llang;
    } else {
        return 'zh_cn';
    }
}

export default (app) => {
    const langstr = sessionStorage.getItem("langAll")
    const langAll = JSON.parse(langstr)
    const dataMsg = {
        zh_cn: langAll?.zh_cn,
        zh_tw: langAll?.zh_tw,
        en: langAll?.en
    }

    const i18n = createI18n({
        legacy: false,
        locale: getDefaultLang(),
        messages: dataMsg
    })

    app.use(i18n)
}