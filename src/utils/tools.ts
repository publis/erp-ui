export const SysID = {
  uuid: (_len: number) => {
    const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
    let uuid = []
    const radix = chars.length
    if (_len) {
      for (let i = 0; i <_len; i++) {
        uuid[i] = chars[0 | (Math.random() * radix)]
      }
    } else {
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-'
      uuid[14] = '4'
      for (let i = 0; i < 36; i++) {
        if (!uuid[i]) {
          let r = 0 | (Math.random() * 16)
          uuid[i] = chars[i === 19 ? (r & 0x3) | 0x8 : r]
        }
      }
    }
    return uuid.join('');
  },
}

const Base64Util = class {
  encode(str: string) {
    return btoa(encodeURI(str))
  }
  decode(str: string) {
    return decodeURI(atob(str))
  }
}

export const encrypt = new Base64Util()