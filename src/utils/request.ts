import axios, { AxiosRequestConfig, AxiosResponse } from 'taro-axios'
import Taro from '@tarojs/taro'
import { cache  } from '@/utils/cache'

const urlPrefix = "/api"

const instance = axios.create({
  // 超时时间 1 分钟
  baseURL: HOST,
  timeout: 30 * 1000,
  headers: {
    'Content-Type': 'application/json;charset=UTF-8',
  },
})

instance.defaults.withCredentials=true

instance.interceptors.request.use((config: AxiosRequestConfig) => {
  const token = cache.get('accessToken')
  let bearer = ``
  if(token != null){
    bearer = `Bearer ${token}`
  }
  config.headers = {
    Authorization: `${bearer}`,
    // 'accessToken': `${token}`,
    ...config.headers,
  }
  return config
})

const showToast = (title: string) => {
  Taro.showToast({
    title,
    icon: 'none',
    duration: 3000,
  })
}
const showMessage = (title: unknown) => {
  let message = JSON.stringify(title)
  message = message === undefined ? '网络请求失败' : message.replace(/"/g, '')
  if (message.indexOf('Network') > -1) {
    showToast('请求失败，请联系客服')
  } else if (message.indexOf('timeout') > -1) {
    showToast('请求超时')
  } else {
    showToast(message)
  }
}
interface ApiResult<T> {
  code: number
  msg?: string
  data: T
}

export function get(url: string, data: any){
  return request<String>({
    url: urlPrefix + url,
    method: 'GET',
    params: data || {}})
}

export function post(url: string, data: any){
  return request<String>({
    url: urlPrefix + url,
    method: 'POST',
    data: data || {}})
}

function request<T>(options: AxiosRequestConfig = {}) {
  if(options.url && options.url.indexOf("/comdict/") == -1) {
    Taro.showLoading({
      title: '加载中...',
      mask: true
    })
  }
  return new Promise<T>((resolve, reject) => {
    instance(options)
      .then((response: AxiosResponse<ApiResult<T>>) => {
        if (response?.status === 200 && response?.data?.code == 0) {
          resolve(response.data.data)
          Taro.hideLoading()
        } else {
          throw response
        }
      })
      .catch((result: AxiosResponse) => {
        if (result?.message && result?.message?.indexOf('401') != -1) {
          showMessage("登录超时")
          Taro.redirectTo({
            url: '/pages/login/index',
          })
        }
        if (result?.status === 200 && result?.data?.code == 100003) {
          cache.remove('accessToken');
          const auth_url = cache.get('AUTH_URL')
          if (cache.get('AUTH_URL')) {
            Taro.redirectTo({ url: auth_url ||'' })
          } else {
            showMessage(result?.data?.msg || result?.msg || result?.data?.message || result)
          }
        } else {
          showMessage(result?.data?.msg || result?.msg || result?.data?.message || result)
        }
        reject(result)
      })
      .finally(() => {
      })
  })
}
