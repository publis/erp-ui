import { encrypt } from '@/utils/tools'

/**
 * 创建本地缓存对象
 * @param {string=} prefixKey -
 * @param {Object} [storage=sessionStorage] - sessionStorage | localStorage
 */
const createStorage = ({ prefixKey = '', storage = sessionStorage } = {}) => {
  /**
   * 本地缓存类
   * @class Storage
   */
  const Storage = class {
    private storage = storage;
    private prefixKey?: string = prefixKey;

    private getKey(key: string) {
      return `${this.prefixKey}${key}`.toUpperCase();
    }

    /**
     * @description 设置缓存
     * @param {string} key 缓存键
     * @param {*} value 缓存值
     */
    set(key: string, value: any) {
      const stringData = JSON.stringify(value);
      this.storage.setItem(this.getKey(key), encrypt.encode(stringData));
    }

    /**
     * 读取缓存
     * @param {string} key 缓存键
     * @param {*=} def 默认值
     */
    get(key: string, def: any = null) {
      const item = this.storage.getItem(this.getKey(key));
      if (item) {
        try {
          return JSON.parse(encrypt.decode(item));
        } catch (e) {
          return def;
        }
      }
      return def;
    }

    /**
     * 从缓存删除某项
     * @param {string} key
     */
    remove(key: string) {
      this.storage.removeItem(this.getKey(key));
    }

    /**
     * 清空所有缓存
     * @memberOf Cache
     */
    clear(): void {
      this.storage.clear();
    }

  };
  return new Storage();
};

export const cache = createStorage();

export const localCache = createStorage({storage: localStorage});

const USER_KEY = 'SESSIONUSER';

export const getUser = () => {
  return cache.get(USER_KEY);
}

export const setUser = (user) => {
  return cache.set(USER_KEY, user)
}

