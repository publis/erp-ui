const isAndroid = window.navigator.userAgent.toLowerCase().indexOf('android') > -1

const isIOS = !!window.navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)

export const SystemUtils = {
  sleep: (delay) => {
    const start = new Date().getTime()
    while (new Date().getTime() < start + delay);
  },
  replaceAll: (str, find, replace) => {
    return str.replace(new RegExp(find, 'g'), replace)
  },
  selectText: (textbox: any, startIndex: number, stopIndex: number) => {
    if (textbox.createTextRange) {
      const range = textbox.createTextRange()
      range.collapse(true)
      range.moveStart('character', startIndex)
      range.moveEnd('character', stopIndex - startIndex)
      range.select()
    } else {
      textbox.setSelectionRange(startIndex, stopIndex)
      textbox.focus()
    }
  },
  copyToClipboard: (str: string) => {
    if (isAndroid) {
      const currentInput = document.createElement('input')
      currentInput.value = str
      document.body.appendChild(currentInput)
      currentInput.select()
      document.execCommand('copy')
      document.body.removeChild(currentInput)
      if (document.execCommand('copy')) {
        // console.log('复制成功')
      } else {
        console.log('复制失败')
      }
    } else if (isIOS) {
      const textString = str.toString()
      let input: HTMLInputElement | null = document.querySelector('#copy-input')
      if (!input) {
        input = document.createElement('input')
        input.id = 'copy-input'
        input.readOnly = true
        input.style.position = 'absolute'
        input.style.top = '-100px'
        input.style.left = '-1000px'
        input.style.zIndex = '-1000'
        document.body.appendChild(input)
      }

      input.value = textString
      SystemUtils.selectText(input, 0, textString.length)
      if (document.execCommand('copy')) {
        document.execCommand('copy')
        // console.log('复制成功')
      } else {
        console.log('复制失败')
      }
      input.blur()
      document.body.removeChild(input)
    } else {
      if (navigator.clipboard && window.isSecureContext) {
        // navigator clipboard api method'
        return navigator.clipboard.writeText(str)
      } else {
        // text area method
        const textArea = document.createElement('textarea')
        textArea.value = str
        // make the textarea out of viewport
        textArea.style.position = 'fixed'
        textArea.style.left = '-999999px'
        textArea.style.top = '-999999px'
        document.body.appendChild(textArea)
        textArea.focus()
        textArea.select()
        return new Promise((res, rej) => {
          // here the magic happens
          document.execCommand('copy') ? res() : rej()
          textArea.remove()
        })
      }
    }
  },
}
