import { createApp } from 'vue'
import { setupStore } from '@/stores'
import setNutUi from './nutui'
import { useSystem } from '@/stores'
import Taro from '@tarojs/taro'
import './app.scss'
import moment from 'moment'
import VConsole from 'vconsole'
import '/public/icon/iconfont.css'
import lang from './language'
import {getLanguage} from '@/api/menu'

getLanguage().then((res) => {
  sessionStorage.setItem("langAll", JSON.stringify(res))
})

const vConsole = new VConsole();

const App = createApp({
  beforeCreate() { 
  },

  mounted() {
  },

  onLaunch(options) {
    const system = useSystem()
    system.init(options)
    system.setInfo(Taro.getSystemInfoSync())
  },

  onShow() {
  },

  onHide() {
  },
})
setNutUi(App)
setupStore(App)
lang(App)

App.config.globalProperties.$filters = {
  formatDate(value) {
    if (value) {
      return moment(String(value)).format('YYYY-MM-DD')
    }
  }
}


export default App
