export default defineAppConfig({

  pages: ['pages/login/index', 'pages/index', 
  'pages/produce/index', 'pages/produce/workOrder/index', 'pages/produce/workOrder/list', 'pages/produce/workOrder/add/index', 
  '/pages/sale/index', 'pages/sale/deliverywarn/index','pages/produce/taskorder/add/index','pages/produce/taskorder/view/index','pages/sale/saleranktop/index',
  'pages/produce/taskorder/index', 'pages/sale/amounttrend/index'],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: '',
    navigationBarTextStyle: 'black',
  },
})
