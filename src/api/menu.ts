import {get} from '@/utils/request'

export const getOneMenus = async () => {
  return await get(`/sys/onemenus`, null)
}

export const getLanguage = async () => {
  return await get(`/sys/language`, null)
}
