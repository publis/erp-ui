import {post, get} from '@/utils/request'


export const login = async (formData) => {
  return await post(`/auth/login`, formData)
}

export const getUserInfo = async () => {
  return await post(`/user/info`, null)
}

export const getUsers = async () => {
  return await get(`/`, null)
}
