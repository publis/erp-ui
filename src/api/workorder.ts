import {get, post} from '@/utils/request'

export const getList = async (params) => {
  return await get(`/workOrder`,params)
}


export const getListTodo = async (params) => {
  return await get(`/workOrder/todo`,params)
}

export const getTaskList = async (params) => {
  return await get(`/taskorder/findlist`,params)
}


export const getTodoTask = async (params) => {
  return await get(`/taskorder/findtodotask`,params)
}

export const getProductionTask = async (params) => {
  return await get(`/taskorder/ptoductiontask`,params)
}

export const getUserInfo = async (params) => {
  return await get(`/sysemployee/searchemployee`,params)
}

export const saveTaskOrder = async (params) => {
  return await post(`/taskorder/save`,params)
}

export const locadInfo = async (params) => {
  return await get(`/taskorder/findguid`,params)
}

export const deleteInfo = async (params) => {
  return await get(`/taskorder/delete`,params)
}

export const audiInfo = async (params) => {
  return await get(`/taskorder/audi`,params)
}

export const cancelAudiInfo = async (params) => {
  return await get(`/taskorder/cancelaudi`,params)
}

