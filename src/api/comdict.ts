import { get } from '@/utils/request'

export const workCenter = {
  getByName: async (params) => {
    return await get(`/comdict/workCenter/findByName`, params)
  }
}

export const machine = {
  getByName: async (params) => {
    return await get(`/comdict/machine/findByName`, params)
  }
}

export const team = {
  getByName: async (params) => {
    return await get(`/comdict/team/findByName`,params)
  }
}


export const saleProduct = {
  getByName: async (params) => {
    return await get(`/comdict/saleProduct/findByName`,params)
  }
}

export const commonDict = {
  search: async (url, params) => {
    return await get(url, params)
  }
}
