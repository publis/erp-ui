import {get} from '@/utils/request'

export const getDeliveryWarnList = async (formSearchFilter) => {
  return await get(`/sales/deliverywarnlist`, formSearchFilter)
}

export const getAmountTrendList = async (formSearchFilter) => {
  return await get(`/sales/amounttrendlist`, formSearchFilter)
}

export const getSaleRankTopList = async (formSearchFilter) => {
  return await get(`/sales/saleranktype`, formSearchFilter)
}
