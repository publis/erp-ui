module.exports = {
  env: {
    NODE_ENV: '"production"',
  },
  defineConstants: {
    HOST: JSON.stringify('./'),
  },
  mini: {},
  h5: {
    publicPath: '../',
    webpackChain (chain, webpack) {
      chain.resolve.mainFields.clear();
      chain.merge({
        resolve: {
          mainFields: ['browser', 'main:h5', 'module', 'main', 'jsnext:main'],
        },
      })
      chain.merge({
        plugin: {
          install: {
            plugin: require('compression-webpack-plugin'),
            args: [{
              test: /\.(js|css)/,
              filename: "[path].gz[query]",
              algorithm: "gzip",
              threshold: 10240,
              minRatio: 0.8,
              deleteOriginalAssets: true,
            }]
          }
        }
      })
    },
  },
}
