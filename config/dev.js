module.exports = {
  env: {
    NODE_ENV: '"development"',
  },
  defineConstants: {
    HOST: JSON.stringify(process.env.CLIENT_ENV === 'h5' ? './' : './'),
  },
  mini: {},
  h5: {
    devServer: {
      open: false,
      proxy: {
        '/api': {
          target: 'http://localhost:8080',
          pathRewrite: {
            '^/api': '/',
          },
          secure: true,
          changeOrigin: true,
        },
      },
    },
  },
}
