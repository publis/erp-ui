const path = require('path')
const args = process.argv
const isOpenDevTools = args.includes('--devtools')

const config = {
  projectName: 'Wx',
  date: '2023-01-01',
  designWidth: 375,
  deviceRatio: {
    640: 2.34 / 2,
    750: 1,
    828: 1.81 / 2,
    375: 2 / 1,
  },
  alias: {
    '@': path.resolve(__dirname, '..', 'src'),
  },
  sourceRoot: 'src',
  outputRoot: 'dist',
  plugins: isOpenDevTools
    ? ['@tarojs/plugin-html', '@tarojs/plugin-vue-devtools', 'taro-plugin-pinia']
    : ['@tarojs/plugin-html', 'taro-plugin-pinia'],
  defineConstants: {},
  sass: {
    resource: [path.resolve(__dirname, '..', 'src/styles/custom_theme.scss')],
    data: `@import "@nutui/nutui-taro/dist/styles/variables.scss";`,
  },
  copy: {
    patterns: [],
    options: {},
  },
  framework: 'vue3',
  mini: {
  },
  h5: {
    publicPath: '/',
    staticDirectory: 'static',
    postcss: {
      autoprefixer: {
        enable: true,
        config: {},
      },
      cssModules: {
        enable: false,
        config: {
          namingPattern: 'module',
          generateScopedName: '[name]__[local]___[hash:base64:5]',
        },
      },
    },
    output: {
      filename: 'js/[name].[hash:8].js',
      chunkFilename: 'js/[name].[chunkhash:8].js',
    },
  },
}

module.exports = function(merge) {
  console.warn(process.env.NODE_ENV)
  if (process.env.NODE_ENV === 'development') {
    return merge({}, config, require('./dev'))
  } else if (process.env.NODE_ENV === 'uat') {
    return merge({}, config, require('./uat'))
  }
  return merge({}, config, require('./prod'))
}
